package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cuenta;

public interface  RepositorioCuentaExtendido {

    public void emparcharCuenta(Cuenta cuenta);
}
