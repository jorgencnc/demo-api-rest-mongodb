package mis.pruebas.apirest.servicios.repositorios;



import mis.pruebas.apirest.modelos.Cuenta;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface RepositorioCuenta extends MongoRepository<Cuenta,String>,RepositorioCuentaExtendido {


    public void deleteById(String numeroCuenta);

}