package mis.pruebas.apirest.servicios.repositorios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioClienteExtendido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Query.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;
import static org.springframework.data.mongodb.core.query.Update.*;

@Repository
public class RepositorioClienteExtendidoImpl implements RepositorioClienteExtendido {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void emparcharCliente(Cliente parche) {
        final Query query = query(where("_id").is(parche.documento));
        final Update update = new Update();

        set(update, "correo", parche.correo);
        set(update, "telefono", parche.telefono);
        set(update, "direccion", parche.direccion);
        set(update, "nombre", parche.nombre);
        set(update, "edad", parche.edad);
        set(update, "fechaNacimiento", parche.fechaNacimiento);

        mongoOperations.updateFirst(query, update, "cliente");
    }

    private void set(Update update, String nombre, Object valor) {
        if(valor != null) {
            System.err.println(String.format("==== MODIFICAR CAMPO %s = %s", nombre, valor));
            update.set(nombre, valor);
        }
    }
}
