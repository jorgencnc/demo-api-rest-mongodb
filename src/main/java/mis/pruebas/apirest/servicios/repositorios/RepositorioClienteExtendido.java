package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cliente;

public interface RepositorioClienteExtendido {

    public void emparcharCliente(Cliente parche);

}
